const april_words = require("@melomario/april-words")

const TOTAL_LIVES = 5
const RUNNING_STATE = "A JOGAR";
const SUCCESS_STATE = "VENCEU!"
const GAME_OVER_STATE = "PERDEU!";

const startGame = () => {
  let word = april_words.getRandomWord();
  return {
    status: RUNNING_STATE,
    word: word,
    lives: TOTAL_LIVES,
    display_word: generateDisplayWord(word, []),
    guesses: []
  };
};

const takeGuess = (game_state, guess) => {
  let new_game_state = {
    ...game_state
  };

  // adicionar guess à lista de guesses
  new_game_state.guesses.push(guess);

  if((typeof new_game_state.word == "string") && !new_game_state.word.includes(guess)){
    
    // tentatica incorrecta
    new_game_state.lives = game_state.lives - 1;


    if (new_game_state.lives == 0) {
      // Game Over
      new_game_state.status = GAME_OVER_STATE
    }

  } else {
    
    // tentativa correta

    // Atualizar display word
    new_game_state.display_word = generateDisplayWord(new_game_state.word, new_game_state.guesses);

    // Success se nao houver "_" na display word
    //if((typeof new_game_state.display_word == "String") && !new_game_state.display_word.includes("_")){
    if(new_game_state.word == new_game_state.display_word.split(' ').join(''))  {
    new_game_state.status = SUCCESS_STATE;
    }
  }

  // retornar novo estado
  return new_game_state;
};

const generateDisplayWord = (word, guesses) => {
  return word.split('').map(letra => {
    return guesses.includes(letra) ? letra + " " : "_ " 
  }).join('').trim()
}

module.exports = {
  startGame,
  takeGuess,
  TOTAL_LIVES,
  RUNNING_STATE,
  SUCCESS_STATE,
  GAME_OVER_STATE
};
