const GameEngine = require("../lib/game_engine");

describe("Nosso jogo garante", () => {
  
  test("um início com um estado inicial válido", () => {
    //Grupo 1
    const initial_state = GameEngine.startGame();
    expect(initial_state.status).toBe(GameEngine.RUNNING_STATE);
    expect(initial_state.word).not.toBe(null);
    expect(initial_state.lives).toBe(GameEngine.TOTAL_LIVES);
    expect(initial_state.guesses.length).toBe(0);
    expect(initial_state.display_word.length).toBe((initial_state.word.length*2)-1);
  });

  test("que o número de chances diminui quando o palpite está errado", () => {
    //Grupo 2
    let initial_state = {
      status: GameEngine.RUNNING_STATE,
      word: "teste",
      lives: 5,
      display_word: "_ _ _ _ _",
      guesses: []
    };

    final_state = GameEngine.takeGuess(initial_state, "r")

    expect(final_state.lives).toBe(4);
    expect(final_state.guesses).toContain("r");
    
  });

  test("que acertos não diminuem o número de chances, mas atualizam o placar", () => {
    //Grupo 1
    let initial_state = {
      status: GameEngine.RUNNING_STATE,
      word: "teste",
      lives: 1,
      display_word: "t _ _ t _",
      guesses: ['a','b','c','d','t']
    };

    initial_guesses = initial_state.guesses.length

    let final_state = GameEngine.takeGuess(initial_state,'e');
    expect(final_state.status).toBe(GameEngine.RUNNING_STATE);
    expect(final_state.lives).toBe(initial_state.lives);
    expect(final_state.guesses.length).toBe(initial_guesses + 1);
    
    //
    expect(final_state.display_word).toBe("t e _ t e");
  });

  test("que o jogo acaba quando as chances se esgotam", () => {
    //Grupo 1
    let initial_state = {
      status: GameEngine.RUNNING_STATE,
      word: "teste",
      lives: 1,
      display_word: "t _ _ t _",
      guesses: ['a','b','c','d','t']
    };
    let final_state = GameEngine.takeGuess(initial_state,'r');
    expect(final_state.status).toBe(GameEngine.GAME_OVER_STATE);
    expect(final_state.lives).toBe(0);
    expect(final_state.guesses.length).toBeGreaterThanOrEqual(GameEngine.TOTAL_LIVES);
    //expect(final_state.guesses.length).toBeGreaterThanOrEquals(TOTAL_LIVES);
  });

  test("que o jogo acaba quando o jogador acerta todas as letras da palavra", () => {
    //Grupo 2
    let initial_state = {
      status: GameEngine.RUNNING_STATE,
      word: "teste",
      lives: 5,
      display_word: "t _ s t _",
      guesses: ["t", "s"]
    };

    final_state = GameEngine.takeGuess(initial_state, "e");
    
    expect(final_state.display_word).toBe("t e s t e");
    expect(final_state.lives).toBeGreaterThan(0);
    expect(final_state.status).toBe(GameEngine.SUCCESS_STATE);
    // letras unicas no final_state.word >= final_state.guesses.length

  });
  
});
